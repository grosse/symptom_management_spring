package org.grosse2011.symptom.generic;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.grosse2011.symptom.repository.CheckIn;

/*
 * This class provides a simple implementation to store image
 * data on the file system. The class provides
 * methods for saving images and retrieving their data.
 * 
 *
 */
public class ImageFileManager {

    /**
	 * This static factory method creates and returns a 
	 * ImageFileManager object to the caller.
	 * 
	 * 
	 */
	public static ImageFileManager get() throws IOException {
		return new ImageFileManager();
	}
	
	private Path targetDir = Paths.get("images");
	
	// The ImageFileManager.get() method should be used
	// to obtain an instance
	private ImageFileManager() throws IOException{
		if(!Files.exists(targetDir)){
			Files.createDirectories(targetDir);
		}
	}
	
	// Private helper method for resolving video file paths
	private String getImageName(CheckIn c) {
		if (c == null) return null;
		return "image" + c.getId() + ".jpg";
	}
	private Path getImagePath(CheckIn checkIn){
		
		if (checkIn == null) return null;
		return targetDir.resolve(getImageName(checkIn));
	}
	
	/**
	 * This method returns true if the specified Video has binary
	 * data stored on the file system.
	 * 
	 */
	public boolean hasImageData(CheckIn c){
		Path source = getImagePath(c);
		return Files.exists(source);
	}
	
	/**
	 * This method copies the binary data for the given image to
	 * the provided output stream. 
	 * 
	*/
	public void copyImageData(CheckIn checkIn, OutputStream out) throws IOException {
		Path source = getImagePath(checkIn);
		if(!Files.exists(source)){
			throw new FileNotFoundException("Unable to find the referenced Image file for imageId:" + checkIn.getId());
		}
		Files.copy(source, out);
	}
	
	/**
	 * This method reads all of the data in the provided InputStream and stores
	 * it on the file system.
	 * 
	*/
	public String saveImageData(CheckIn checkIn, InputStream imageData) throws IOException{
		assert(imageData != null);
		
		Path target = getImagePath(checkIn);
		Files.copy(imageData, target, StandardCopyOption.REPLACE_EXISTING);
		return getImageName(checkIn);
	}
	
}


