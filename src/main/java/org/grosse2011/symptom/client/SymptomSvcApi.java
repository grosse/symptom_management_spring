package org.grosse2011.symptom.client;

import java.util.Collection;

import javax.annotation.security.RolesAllowed;

import org.grosse2011.symptom.generic.ImageStatus;
import org.grosse2011.symptom.repository.CheckIn;
import org.grosse2011.symptom.repository.CheckInDto;
import org.grosse2011.symptom.repository.DoctorDto;
import org.grosse2011.symptom.repository.PainMedicationDto;
import org.grosse2011.symptom.repository.PatientDto;
import org.grosse2011.symptom.repository.PrescriptionDto;



import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.PUT;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;

public interface SymptomSvcApi {
    public static final String PATIENT_SVC_PATH = "/patients";
    public static final String TOKEN_PATH = "/oauth/token";
    public static final String DOCTOR_SVC_PATH = "/doctors";
    public static final String PAIN_MEDICATION_SVC_PATH = "/painMedications";
    public static final String GET_PRESCRIPTION_BY_PATIENT = PATIENT_SVC_PATH + "/{id}/prescriptions";
    public static final String CHECKINS_BY_PATIENT = PATIENT_SVC_PATH + "/{id}/checkIns";
    public static final String MEDICATION_BY_PRESCRIPTION_BY_PATIENT = PATIENT_SVC_PATH + "/{id}/prescriptions/Medication";
    public static final String ADD_PRESCRIPTION_BY_PATIENT=	PATIENT_SVC_PATH + "/{id}/prescriptions/{side}";	 
    public static final String PATIENTS_TO_ALERT_BY_DOCTOR = DOCTOR_SVC_PATH + "/{id}/patientsToAlert/{hours}";
    public static final String PATIENTS_BY_DOCTOR_PATH= DOCTOR_SVC_PATH + "/{id}/patients";
    public static final String IMAGE_CHECKIN_PATH = "/checkIns/{id}/images";
    public static final String CHECKIN_SVC_PATH = "/checkIns";
      
   
    @POST(PATIENT_SVC_PATH)
    public PatientDto addPatient(@Body PatientDto patient);

    @GET(SymptomSvcApi.PATIENTS_TO_ALERT_BY_DOCTOR)
    public Collection<PatientDto> getPatientsToAlert(@Path("id") long id, @Path("hours") int hours);

    
    @GET(SymptomSvcApi.PATIENT_SVC_PATH) 
    public Collection<PatientDto> getPatients();

    @GET(SymptomSvcApi.CHECKINS_BY_PATIENT)
    public Collection<CheckInDto> getCheckInsByPatient(@Path("id") long id);

    @POST(SymptomSvcApi.ADD_PRESCRIPTION_BY_PATIENT)
    public boolean createPrescriptionByPatient(@Path("id") long id);
 
    @PUT(SymptomSvcApi.ADD_PRESCRIPTION_BY_PATIENT)
    public PatientDto upsertPrescriptionForPatient(@Path("id") long id, @Body PrescriptionDto prescription,  @Path("side") String side);
  
    @GET(SymptomSvcApi.GET_PRESCRIPTION_BY_PATIENT) 
    public PrescriptionDto getPrescriptionByPatient(@Path("id") long id);

    @GET(SymptomSvcApi.GET_PRESCRIPTION_BY_PATIENT + "/side") 
    public PrescriptionDto getPrescriptionByPatientSide(@Path("id") long id);

    @POST(SymptomSvcApi.CHECKINS_BY_PATIENT)
    public CheckInDto addCheckInByPatient(@Path("id") long id, @Body CheckInDto checkIn);

    @GET(PAIN_MEDICATION_SVC_PATH)
    public Collection<PainMedicationDto> getPainMedications();

    @POST(PAIN_MEDICATION_SVC_PATH)
    public boolean addPainMedication(@Body PainMedicationDto painMedicationDto);

    @POST(DOCTOR_SVC_PATH)
    public DoctorDto addDoctor(@Body DoctorDto doctorDto);

    @POST(value=DOCTOR_SVC_PATH+"/{id}/addPatient")
    public PatientDto addPatientToDoctor(@Path("id") long doctorId, @Body PatientDto patientDto);

    @GET(value=DOCTOR_SVC_PATH) 
    public Collection<DoctorDto> getDoctors(); 

    @GET(value=DOCTOR_SVC_PATH + "/{id}")
    public DoctorDto getDoctor(@Path("id") long id);

    @GET(value=PATIENT_SVC_PATH + "/{id}")
    public PatientDto getPatient(@Path("id") long id);

    @GET(value=PATIENTS_BY_DOCTOR_PATH) 
    public Collection<PatientDto> getPatientsByDoctor(@Path("id") long id); 
   
    
    @GET(CHECKIN_SVC_PATH)
    public Collection<CheckIn> getCheckInList();
		
    @POST(CHECKIN_SVC_PATH)
    public CheckInDto addCheckIn(@Body CheckIn checkIn);
    
    @Multipart
    @POST(IMAGE_CHECKIN_PATH)
    public ImageStatus addImageToCheckIn(@Path("id") long id, @Part("data") TypedFile imageData);

    @GET(IMAGE_CHECKIN_PATH)
    Response getImageForCheckIn(@Path("id") long id);
		
}
    