package org.grosse2011.symptom.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.common.base.Objects;

/*
 This object encapsulate a patient checkin
 There are Answers to the questions:
    Can you eat?
    How much pain are you in?
    There is also a List of medicationsTaken, the time taken, and whether they were taken
    An optional filename for a image of patient mouth sores
    -Eric
 */

@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
@Entity
public class CheckIn {
    /* Patient can have 3 levels of pain, this is what we want to track 
    /* Use enumerated type for each pain level 
     * 
     */
    public enum Pain {
    	WELL_CONTROLLED,
    	MODERATE,
    	SEVERE
    }
    /* Keep track of whether patient stops eating as an enumerated type
     *  */
    
    public enum StoppedEating {
    	NO,
    	SOME,
    	I_CANT_EAT
    }
   
       @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @Enumerated(EnumType.STRING)
    private Pain pain;
       
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="patientId")
    private Patient patient;
       
    private long date;
    private String imageName;
    
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy="checkin")
    private Collection<MedicationTaken> medicationsTaken= new ArrayList<MedicationTaken>();
    
    @Enumerated(EnumType.STRING)
    private StoppedEating isEating;
    
    public long getDate() {
       	return this.date;
    }
       
    public void setDate(long date) {
       	this.date = date;
    }
       
    public Collection<MedicationTaken> getMedicationsTaken() {
       	return this.medicationsTaken;
    }
    
    public void AddMedicationsTaken(Collection<MedicationTaken> medicationsTaken) {
      	this.medicationsTaken= medicationsTaken;
    }
    
    public CheckIn() {
      	this.imageName = null;
    }
    
    public void setIsEating(StoppedEating isEating) {
       	this.isEating = isEating;
    }
    
    public StoppedEating getIsEating() {
       	return this.isEating;
    }

    public void setImageName(String name) {
        this.imageName = name;
    }

    public String getImageName() {
     	return this.imageName;
    }
    
    /* associate a medicationTaken with this CheckIn, and associate this checkIn with the medication 
    * Taken for consistency
    */
    public void addMedicationTaken(MedicationTaken medicationTaken) {
       	if (!medicationsTaken.contains(medicationTaken)) { 
       		medicationsTaken.add(medicationTaken);
       		medicationTaken.setCheckIn(this);
        }
    }
       
    /* disassociate a medicationTaken from this CheckIn, and disassociate this checkIn from the medication
     * Taken for consistency 
    */
    
    public void removeMedicationTaken(MedicationTaken medicationTaken)
    {
       	if (medicationsTaken.contains(medicationTaken)) {
       		medicationsTaken.remove(medicationTaken);
       		medicationTaken.setCheckIn(null);
       	}
    }	
     
    
    public Pain getPain() {
        return this.pain;
    }
    
    
    public Patient getPatient() {
      	return this.patient;
    }
       
    public void setPain(Pain pain) {
       	this.pain = pain;
    }
    
    
    /* associate patient with this checkIn, and associate this checkIn with the patient for 
     * consistency
    */
    public void setPatient(Patient patient) {
    	/* Check I am not trying to set patient to existing patient */
       	if (same(patient)) return; 
    	
    	Patient oldPatient = this.patient;
    	this.patient = patient;
    	// update patient for consistency
    	if (oldPatient != null) oldPatient.removeCheckIn(this);
        if (patient != null) patient.addCheckIn(this);
    	
    }
    /* return true if this patient and other patient are equal, or are both null
     * 
     */
    private boolean same(Patient newPatient ) {
    	if (this.patient == null) return newPatient == null;
    	else return this.patient.equals(newPatient);
    	
      }

      
    public long getId() {
    	return id;
    }

    public void setId(long id) {
    	this.id = id;
    }

    /**
     * Two CheckIns generate the same hashcode if they have exactly the same
     * values for their id.
     * 
     */
    @Override
    public int hashCode() {
    	// Google Guava provides great utilities for hashing
    	return Objects.hashCode(id);
    }

    /**
     * Two CheckIns are considered equal if they have exactly the same values for
     * their id
     * 
     */
    @Override
    public boolean equals(Object obj) {
    	if (obj instanceof CheckIn) {
    		CheckIn other = (CheckIn) obj;
    		// Google Guava provides great utilities for equals too!
    		return Objects.equal(id, other.id);
    			
    	} 
    	else {
    		return false;
    	}
    }
}
