package org.grosse2011.symptom.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.google.common.base.Objects;

@Entity
public class PainMedication {
    	 @Id
	   @GeneratedValue(strategy = GenerationType.AUTO)
	   private long id;
	   @Column(unique=true)
	   private String name;
	   
	   @OneToMany (cascade = CascadeType.ALL, mappedBy="medication")
	   private Collection<MedicationTaken> medicationsTaken= new ArrayList<MedicationTaken>();
	   
	   
	   public String getName() {
		   return this.name;
		  
	   }
	   public void setName(String name) {
		   this.name = name;
	   }
	   public long getId() {
		   return this.id;
		  
	   }
	   public void setId(long id) {
		   this.id = id;
	   }
	   public PainMedication() {
		
	   }
	   
	   /* Construct a pain Medication from a Pain Medication facade */
	   
	   public PainMedication(PainMedicationDto p) {
		   this.name = p.name;
		   this.id = p.id;
	   }
	   
	   
	   public void addMedicationTaken(MedicationTaken medicationTaken) {
			if (medicationsTaken.contains(medicationTaken)) return;
			medicationsTaken.add(medicationTaken);
			medicationTaken.setMedication(this);
		}
		
		public void removeMedicationTaken(MedicationTaken medicationTaken) {
		    
		    if (!medicationsTaken.contains(medicationTaken)) return ;
		    
		    medicationsTaken.remove(medicationTaken);
		    medicationTaken.setMedication(null);
		  }
		
	   
		@Override
		public int hashCode() {
			return Objects.hashCode(name);	
		}

		/**
		 * Two PainMedications are considered equal if they have values for name 
		 * 
		 */
		//@Override
		public boolean equals(Object obj) {
			if (obj instanceof PainMedication) {
				PainMedication other = (PainMedication) obj;
				// Google Guava provides great utilities for equals too!
			return Objects.equal(name, other.name);
					
			} 
			else {
				return false;
			}
		}

}
