package org.grosse2011.symptom.controllers;



// HTTP code repsonse
// better error handling, perhaps JSON
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.grosse2011.symptom.client.SymptomSvcApi;
import org.grosse2011.symptom.generic.ImageFileManager;
import org.grosse2011.symptom.generic.ImageStatus;
import org.grosse2011.symptom.generic.Utilities;
import org.grosse2011.symptom.repository.CheckIn;
import org.grosse2011.symptom.repository.CheckInDto;
import org.grosse2011.symptom.repository.CheckInRepository;
import org.grosse2011.symptom.repository.Doctor;
import org.grosse2011.symptom.repository.DoctorDto;
import org.grosse2011.symptom.repository.DoctorRepository;
import org.grosse2011.symptom.repository.MedicationTaken;
import org.grosse2011.symptom.repository.MedicationTakenDto;
import org.grosse2011.symptom.repository.MedicationTakenRepository;
import org.grosse2011.symptom.repository.PainMedication;
import org.grosse2011.symptom.repository.PainMedicationDto;
import org.grosse2011.symptom.repository.PainMedicationRepository;
import org.grosse2011.symptom.repository.Patient;
import org.grosse2011.symptom.repository.PatientDto;
import org.grosse2011.symptom.repository.PatientRepository;
import org.grosse2011.symptom.repository.Prescription;
import org.grosse2011.symptom.repository.PrescriptionDto;
import org.grosse2011.symptom.repository.PrescriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import retrofit.client.Response;
import retrofit.mime.TypedFile;

import com.google.common.collect.Lists;
@Controller
public class SymptomSvc implements SymptomSvcApi {
    @Autowired
    private PatientRepository patients;
    @Autowired
    private CheckInRepository checkins;
    @Autowired
    private PrescriptionRepository prescriptions;
    @Autowired
    private PainMedicationRepository medications;
    @Autowired
    private MedicationTakenRepository medicationsTaken;
    @Autowired
    private DoctorRepository doctors;
    
    private ImageFileManager mImageFileManager;
    
    /*
    Add a patient to the Patient repository "table"
    */
    
    @RequestMapping(value=SymptomSvcApi.PATIENT_SVC_PATH, method=RequestMethod.POST)
    public @ResponseBody PatientDto addPatient(@RequestBody PatientDto patientDto){
        Patient patient = new Patient(patientDto);
        patient = patients.save(patient);
        return new PatientDto(patient);
    }
    /*
     *     (non-Javadoc)
     * 
     *   @see org.grosse2011.symptom.client.SymptomSvcApi#getPatients()
     *   
     *    Return the contents of the patient repository as a 
     *    Collection of PatientDto's
     * 
     */
    
    @RequestMapping(value=SymptomSvcApi.PATIENT_SVC_PATH, method=RequestMethod.GET) 
    public     @ResponseBody Collection<PatientDto> getPatients() {
    	// Create a Collection of PatientDto's based on the Patient repository
    	
        Collection<PatientDto> patientdtos = new ArrayList<PatientDto>();
        for(Patient p : patients.findAll()) {
            patientdtos.add( new PatientDto(p));
        }
    	
        return patientdtos;
    }      
    /*
     * (non-Javadoc)    
     *  @see org.grosse2011.symptom.client.SymptomSvcApi#getCheckInsByPatient(long)
     *  Given a patient id, return a Collection of CheckinDtos for that patient
    */
    @RequestMapping(value=SymptomSvcApi.CHECKINS_BY_PATIENT, method=RequestMethod.GET) 
    public @ResponseBody Collection<CheckInDto> getCheckInsByPatient(@PathVariable long id) {
        Collection<CheckInDto> checkInDtos = new ArrayList<CheckInDto>();
        Patient patient = patients.findOne(id);
        if (patient == null) return null; // 404
        Collection<CheckIn> checkIns = patient.getCheckIns();
        if (checkIns == null) return null; // TODO 404 ??
            for(CheckIn checkIn : checkIns) {
                CheckInDto checkInDto = new CheckInDto(checkIn);
                System.out.println(checkInDto.json());
                checkInDtos.add(new CheckInDto(checkIn));
            }
        return checkInDtos;
    }      
    /*  
     * (non-Javadoc)
     * @see org.grosse2011.symptom.client.SymptomSvcApi#createPrescriptionByPatient(long)
     * Given a patient id, create an associated prescription if no prescription exists
     * return false if this patient already has a prescription
     */
    @RequestMapping(value=SymptomSvcApi.ADD_PRESCRIPTION_BY_PATIENT, method=RequestMethod.POST) 
    public @ResponseBody boolean createPrescriptionByPatient(@PathVariable long id) {
        Patient patient = patients.findOne(id);
        if (patient == null) return false; // TODO 404
        if (patient.getPrescription() != null) return false; // TODO 400  
        Prescription prescription = new Prescription();
        prescription.setPatient(patient);
        prescriptions.save(prescription);
        return true;
    }	
    
    /*
     * (non-Javadoc)
     * @see org.grosse2011.symptom.client.SymptomSvcApi#upsertPrescriptionForPatient(long, org.grosse2011.symptom.repository.PrescriptionDto, java.lang.String)
     * Given a patient id and a json encoded prescription list, update or insert the prescription
     * list for the associated patient in the precription
     */
    @RequestMapping(value = SymptomSvcApi.ADD_PRESCRIPTION_BY_PATIENT, method=RequestMethod.PUT)
    public @ResponseBody PatientDto upsertPrescriptionForPatient(@PathVariable("id") long id, @RequestBody PrescriptionDto prescriptionDto,  @PathVariable("side") String side) {
        System.out.println("upsertPrescrptionForPatient ( " + Long.toString(id) +  " , " + prescriptionDto.json() + " )");
        Prescription pre = new Prescription(prescriptionDto);
    	
        Patient patient = patients.findOne(id);
        // patient doesnt' exist	
        if (patient == null) {
            return null;
        }
    
        patient.setPrescription(new Prescription());
        for (PainMedicationDto painMedicationDto : prescriptionDto.getPainMedications()) {
            patient.getPrescription().addMedication(getMedicationByName(painMedicationDto.name));
        }
     
        Prescription prescription = prescriptions.save(patient.getPrescription());
        PrescriptionDto pdto = new PrescriptionDto(pre);
        System.out.println(pdto.json());
        // set comma delimited list
        // workaround for many-many issues  
        patient.setSide(side);
        patient = patients.save(patient);
    	    
        if (patient == null) return null;
        else return new PatientDto(patient);
        }
    		
    @RequestMapping(value=SymptomSvcApi.PATIENT_SVC_PATH+"/{id}/checkins", method=RequestMethod.GET)
    public @ResponseBody Collection<CheckIn> checkins(@PathVariable long id, @RequestBody CheckIn checkIn){
    	
        ArrayList<CheckIn> newCheckIns = new ArrayList<CheckIn>();
        for (CheckIn c : checkins.findAll()) if (c.getPatient().getId() == id) newCheckIns.add( c);
        return newCheckIns;
    }
    /* Given a patient id, return the associated prescription and return it as a json encoded 
     * ojbect	
     * * @see org.grosse2011.symptom.client.SymptomSvcApi#getPrescriptionByPatient(long)
     */
    @RequestMapping(value=SymptomSvcApi.GET_PRESCRIPTION_BY_PATIENT, method=RequestMethod.GET) 
    public @ResponseBody PrescriptionDto getPrescriptionByPatient(@PathVariable long id) {
    	
        Patient patient = patients.findOne(id);
        if (patient == null) {
            return null; // TODO 400
        }
    	  
        Prescription prescription  = patient.getPrescription();
        if (prescription == null) {
            return null; // TODO 404
        }
        return new PrescriptionDto(prescription);
    }
    /* Given a patient id, return the associated prescription ( encoded as a comma delimited string) and return it as a json encoded 
     * object -- This should be changed later	
     * @see org.grosse2011.symptom.client.SymptomSvcApi#getPrescriptionByPatient(long)
     */
    @RequestMapping(value=SymptomSvcApi.GET_PRESCRIPTION_BY_PATIENT + "/side", method=RequestMethod.GET) 
    public @ResponseBody PrescriptionDto getPrescriptionByPatientSide(@PathVariable long id) {
    	        
        Patient patient = patients.findOne(id);
        if (patient == null) {
            return null; // TODO 400
        }
    	  
        Prescription prescription  = patient.getPrescription();
        if (prescription == null) {
            return null; // TODO 404
        }
        String side = patient.getSide();
        long prescription_id = patient.getId();
        Prescription cur_prescription = Utilities.sideToPrescription(side, prescription_id);
        return new PrescriptionDto(cur_prescription);
    }	
    	       
    	
    /* Given a Patient id and json encoded ChecInkDto, create a Checkin object, assocatiate it with the correct patient,
     * update checkIn reposititory with new checkIn 
     * (non-Javadoc)
     * @see org.grosse2011.symptom.client.SymptomSvcApi#addCheckInByPatient(long, org.grosse2011.symptom.repository.CheckInDto)
     */
    
    @RequestMapping(value=SymptomSvcApi.CHECKINS_BY_PATIENT, method=RequestMethod.POST)
    public @ResponseBody CheckInDto addCheckInByPatient(@PathVariable long id, @RequestBody CheckInDto checkInDto) {
    
        CheckIn checkIn = new CheckIn();
        Patient patient = patients.findOne(id);
        checkIn.setPatient(patient);
        checkIn.setPain(checkInDto.pain);
        checkIn.setIsEating(checkInDto.isEating);
        checkIn.setDate(checkInDto.date);
    	
        if (checkInDto.medicationsTaken != null) { 
            for (MedicationTakenDto medTakenDto : checkInDto.medicationsTaken) {
    		
                PainMedication med = medications.findByName(medTakenDto.medication.name);
                if (med == null) return null;
                MedicationTaken medTaken = new MedicationTaken(med);
                medTaken.setHasTaken(medTakenDto.hasTaken);
                medTaken.setWhenTaken(medTakenDto.whenTaken);
                checkIn.addMedicationTaken(medTaken); // TODO add date to addMedication
            }
        }
    
        patient.addCheckIn(checkIn);
        checkins.save(checkIn);
    	
        return new CheckInDto(checkIn);
    }
    
    /* Return all pain medications as a Collection of PainMedicationDto's
     * (non-Javadoc)
     * @see org.grosse2011.symptom.client.SymptomSvcApi#getCheckInsByPatient(long)
     */
    	
    @RequestMapping( value=SymptomSvcApi.PAIN_MEDICATION_SVC_PATH, method=RequestMethod.GET)
    public @ResponseBody Collection<PainMedicationDto> getPainMedications() {
        Collection<PainMedicationDto> medicationDtos = new ArrayList<PainMedicationDto>();
        for (PainMedication medication :  medications.findAll())	{
            PainMedicationDto medicationDto = new PainMedicationDto(medication.getId(), medication.getName());
            medicationDtos.add(medicationDto);
        }
    	return medicationDtos;
    }
    	
    /* 
     * Given a JSON encoded painMediationDto add, 
     * create a PainMedication and add it the the Pain medication repository
     * (non-Javadoc)
     * @see org.grosse2011.symptom.client.SymptomSvcApi#addPainMedication(org.grosse2011.symptom.repository.PainMedicationDto)
     */
    	 
    @RequestMapping( value=SymptomSvcApi.PAIN_MEDICATION_SVC_PATH, method=RequestMethod.POST)
    public @ResponseBody boolean addPainMedication(@RequestBody PainMedicationDto painMedicationDto) {
    		
        PainMedication painMedication = new PainMedication(painMedicationDto);
        painMedication = medications.save(painMedication);
        return painMedication != null; 
    }
    	
    
    /*    
    Given a JSON encoded doctorDto, create a Doctor and add it to the "Doctors" repository
    */
        	
    @RequestMapping(value=SymptomSvcApi.DOCTOR_SVC_PATH, method=RequestMethod.POST)
    public @ResponseBody DoctorDto addDoctor(@RequestBody DoctorDto doctorDto){
    	
        Doctor doctor = new Doctor(doctorDto);
        doctor = doctors.save(doctor);
        DoctorDto cur_doctorDto = new DoctorDto(doctor);
        return cur_doctorDto;		
    }
    /* Given a doctor id and a JSON encoded patientDto, create a patient and associate it with the given doctor

     */
    @RequestMapping(value=SymptomSvcApi.DOCTOR_SVC_PATH+"/{id}/addPatient", method=RequestMethod.POST)
    public @ResponseBody PatientDto addPatientToDoctor(@PathVariable("id") long doctorId, @RequestBody PatientDto patientDto)	{
    		
        Doctor doctor = doctors.findOne(doctorId);
        if (doctor==null) {
            return null;
        }
        // find patient
        Patient curPatient = null;
        // Find patient based on medicalRecordNo
        for (Patient patient : patients.findAll()) {
            if (patient.getMedicalRecordNo() == patientDto.medicalRecordNo) {
                curPatient = patient;
            }
        }
        // Patient doesn't exist
    		
        if (curPatient == null) {
            return null;
        }
    		
        doctor.addPatient(curPatient);
        doctors.save(doctor);
    	
        return new PatientDto(curPatient);
    }
    	
    /* Given a doctor id return a JSON encoded doctorDto
     * (non-Javadoc)
     * @see org.grosse2011.symptom.client.SymptomSvcApi#getDoctors()
     */
    @RequestMapping(value=SymptomSvcApi.DOCTOR_SVC_PATH + "/{id}", method=RequestMethod.GET) 
    public @ResponseBody DoctorDto getDoctor(@PathVariable("id") long doctorId) {
    	
        Doctor doctor = doctors.findOne(doctorId);
        if (doctor == null) {
            return null;
        }
        DoctorDto doctorDto = new DoctorDto(doctor.getId(), doctor.getFirstName(), doctor.getLastName());
        return doctorDto;
    }
    
    	
    		
    /*
     *  Get all doctors and return as a collection of DoctorDto's 
     */
    @RequestMapping(value=SymptomSvcApi.DOCTOR_SVC_PATH, method=RequestMethod.GET) 
    public @ResponseBody Collection<DoctorDto> getDoctors() {
        Collection<DoctorDto> doctorDtos = new ArrayList<DoctorDto>();
        for(Doctor doctor : doctors.findAll()) {
            doctorDtos.add(new DoctorDto(doctor));
        }
    
        return doctorDtos;
    }
    	
    /*
     * Given a patient id return a JSON encode patientDto
     ** (non-Javadoc)
     **/
    @RequestMapping(value=SymptomSvcApi.PATIENT_SVC_PATH + "/{id}", method=RequestMethod.GET) 
    public @ResponseBody PatientDto getPatient(@PathVariable("id") long patientId) {
        PatientDto patientDto = null;
        Patient patient = patients.findOne(patientId);
        if (patient == null) {
            return null;
        }
    	
        patientDto = new PatientDto(patient);
        return patientDto;
    }
    	
    /* Given doctor id return a Json encoded Collection of PatientDto's associated with 
     * that doctor
     * (non-Javadoc)
     * @see org.grosse2011.symptom.client.SymptomSvcApi#getPatientsByDoctor(long)
     */
    	
    @RequestMapping(value=SymptomSvcApi.PATIENTS_BY_DOCTOR_PATH, method=RequestMethod.GET)     
    public @ResponseBody Collection<PatientDto> getPatientsByDoctor(@PathVariable long id) {
        Collection<PatientDto> patientDtos = new ArrayList<PatientDto>();
        Doctor doctor = doctors.findOne(id);
        if  (doctor == null) return null; //TODO 404
        for (Patient patient : doctor.getPatients()) {
            PatientDto patientDto = new PatientDto(patient);
            patientDtos.add(patientDto);
        }
        return patientDtos;
    }
    
    /*
     * (non-Javadoc)
     * @see org.grosse2011.symptom.client.SymptomSvcApi#getCheckInList()
     * Returns a JSON encoded collection of all checkIns
     */
    @RequestMapping(value=SymptomSvcApi.CHECKIN_SVC_PATH, method=RequestMethod.GET)
    public @ResponseBody Collection<CheckIn> getCheckInList(){
    	return Lists.newArrayList(checkins.findAll());	
    }
    
    /*
     *  Given a Json encoded CheckIn, add it to the repository
     *  This is for testing
     */
    
    @RequestMapping(value=SymptomSvcApi.CHECKIN_SVC_PATH, method=RequestMethod.POST)
    public @ResponseBody CheckInDto addCheckIn(@RequestBody CheckIn checkIn) {
    	  
    	CheckIn cur_checkIn = checkins.save(checkIn);
    	if (cur_checkIn == null) return null;
    	CheckInDto checkInDto = new CheckInDto(cur_checkIn);
    	return checkInDto;
    }
    		
  
    /* Given a checkIn id, and a bytestream of image data, save the data onto the sd card
     * and assocate the file with the checkin
     */
    @RequestMapping(value=SymptomSvcApi.IMAGE_CHECKIN_PATH, method=RequestMethod.POST)
    public @ResponseBody ImageStatus addImageToCheckIn(@PathVariable("id") long id, @RequestParam("data") MultipartFile imageData) {
    		  
        ImageStatus imageStatus = new ImageStatus(ImageStatus.ImageState.READY);
        CheckIn checkIn  = checkins.findOne(id);
        if (checkIn == null) {
    		return imageStatus;
        }
        try {
            mImageFileManager = ImageFileManager.get();
            String imageName = mImageFileManager.saveImageData(checkIn, imageData.getInputStream());
            checkIn.setImageName(imageName);
            CheckIn curCheckIn = checkins.save(checkIn);
            //CheckInDto checkInDto = new CheckInDto(cur_checkIn);
        }
        catch  (IOException e)	{
    					
            return imageStatus;
        };
        return imageStatus;  
    }
    /* Make the type checker happy with TypedFile on the jackson side
     * (non-Javadoc)
     * @see org.grosse2011.symptom.client.SymptomSvcApi#addImageToCheckIn(long, retrofit.mime.TypedFile)
     */
    @Override
    public ImageStatus addImageToCheckIn(long id, TypedFile imageData) {
        // TODO Auto-generated method stub
        return null;
    }

    
    /*
     * Given a Doctor id, return a JSON encoded list of patients who need to be alerted 
     * due severity of their pain
     * 
     * checkIn criteria over the last 24 hours:
     * SERVERE pain for 12 hours
     * NOT ABLE TO EAT for 12 hours
     * MODERATE pain for 18 hours
     * (non-Javadoc)
     * @see org.grosse2011.symptom.client.SymptomSvcApi#getPatientsToAlert(long, int)
     */
    @RequestMapping(value=SymptomSvcApi.PATIENTS_TO_ALERT_BY_DOCTOR, method=RequestMethod.GET)
    public @ResponseBody Collection<PatientDto> getPatientsToAlert(@PathVariable("id") long id, @PathVariable("hours") int hours) {
    	Date date = new Date();
    	Collection<CheckIn> checkIns = checkins.findByDoctorByDateGreaterThanOrderByDate(id, date.getTime() - (hours * Utilities.MILLIS_TO_HOURS));
    	Collection<PatientDto> patientDtos = Utilities.getAllAlerts(checkIns);
    	return patientDtos;
    }
    	
    /* Given a checkIn id, use the fileMangager object to find the associated
     * image, and return as byte stream. If file not found or an error
     */
    @RequestMapping(value=SymptomSvcApi.IMAGE_CHECKIN_PATH, method=RequestMethod.GET)  
    public void getImageForCheckIn(@PathVariable("id") long id,  HttpServletResponse response) {
         
        CheckIn checkin = checkins.findOne(id);
        if (checkin != null) {
            try {
                mImageFileManager = ImageFileManager.get();
                mImageFileManager.copyImageData(checkin, response.getOutputStream());
            } catch (IOException e) {
                response.setStatus(404);
                return;
            };
        }
        else {
            response.setStatus(404);
        }
    }
    
    	
    @Override
    public Response getImageForCheckIn(long id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    /* helper method name is self explanatory */
    
    private PainMedication getMedicationByName(String name) {
        for (PainMedication painMedication : medications.findAll()) {
            if (painMedication.getName().equals(name)) return painMedication;
        }
        return null;  
    }
}



