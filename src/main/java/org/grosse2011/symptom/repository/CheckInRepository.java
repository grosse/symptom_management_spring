package org.grosse2011.symptom.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * An interface for a repository that can store Video 
 * objects and allow them to be searched by title.
 * 
 * @author jules
 *
 */
@Repository
public interface CheckInRepository extends CrudRepository<CheckIn, Long>{

	// Find all videos with a matching title (e.g., Video.name)                             
	@Query(value = "select c from CheckIn c where c.date > ?2 and c.patient.doctor.id = ?1 order by c.date")
	public Collection<CheckIn> findByDoctorByDateGreaterThanOrderByDate(Long id, Long date);
	//public Collection<CheckIn> findByDoctorId(long DoctorId);
	//public Collection<CheckIn> findByDoctorIdAndPatientId(long DoctorId, long PatientId);
}
