package org.grosse2011.symptom.repository;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.common.base.Objects;
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
@Entity
public class Patient {
	  
		    
		   	@Id
			@GeneratedValue(strategy = GenerationType.AUTO)
			private long id;
		   	
		   	@ManyToOne(cascade=CascadeType.ALL)
			@JoinColumn(name="doctorId")
			private Doctor doctor;
		   	
		   	@Column(unique=true)
		   	private long medicalRecordNo;
		   	
		   	private long birthDate;
		   	public void setBirthDate(long d) { this.birthDate = d; }
		   	public long getBirthDate() { return this.birthDate; }
		   	private String firstName;
		   	private String lastName;
		   	private String side;
		   
		   	@OneToMany(mappedBy="patient")
		   	private Collection<CheckIn> checkIns = new ArrayList<CheckIn>();
		   	
		   	@OneToOne(mappedBy="patient")
		   	private Prescription prescription;
		   	
		   	
		     	
		   	
		   	//@OneToMany
		    //@JoinColumn(name="patientId", referencedColumnName="Id")
		    //private Collection<CheckIn> checkIns = new ArrayList<CheckIn>();
	
			
			public Patient() {
			}
			
			/* Create a Patient object from a 
			 * Patient details object
			 */
			
			public Patient(PatientDto patientDto) {
				this.id = patientDto.id;
				this.firstName = patientDto.firstName;
				this.lastName = patientDto.lastName;
				this.medicalRecordNo = patientDto.medicalRecordNo;
			    this.birthDate = patientDto.birthDate;
			
			}
			
			public void setId(long id) {
				this.id = id;
			}
			
			public long getId() {
				return this.id;
			}
			/* These are a kluge to bypass prescription
			 * many-many issue, isde is a csv string of medications
			 * for the patients prescription -- this should be fixed
			 */
			
			public void setSide(String side) { this.side = side; }
		   	public String getSide() { return this.side; }
		   	
			/* Return the CheckIns for this patient as a 
			 * Collection of CheckIns
			
			 */
			public Collection<CheckIn> getCheckIns() {
			   
			    return new ArrayList<CheckIn>(this.checkIns);
			  }
			
			/* Add a CheckIn for this patient,
			 * Make sure checkIn properly references
			 * this patient
			 */
			
			public void addCheckIn(CheckIn checkIn)  {
			  
			    if (!checkIns.contains(checkIn))       {
		    		    
			    checkIns.add(checkIn);
			    checkIn.setPatient(this);
			  }
			}
			
			/* Remove CheckIn from this objects CheckIn list,
			 * and null out its patient reference
			 */
			
			public void removeCheckIn(CheckIn checkIn) {

				    //if (checkIns.contains(checkIn));
				   checkIns.remove(checkIn);
				   checkIn.setPatient(null);
			}
			  
			public String getFirstName() {
				return this.firstName;
			}
			
			public String getLastName()			{
				return this.lastName;
			}
			
			public void setFirstName(String firstName) {
				this.firstName = firstName;
				return;
			}
			public void setLastName(String lastName) {
				this.lastName = lastName;
				return;
			}
			
			/* Get this patient's one and only doctor and
			 * return it as a details object
			 */
			
			public DoctorDto getDoctor() {
				    if (this.doctor == null) return null;
				    Doctor doctor = this.doctor;
				    DoctorDto doctorDto = new DoctorDto(doctor.getId(), doctor.getFirstName(), doctor.getLastName());
					return doctorDto;
			}
			
			public Prescription getPrescription() {
				return this.prescription;
			}
			
			public long getMedicalRecordNo() {
				return this.medicalRecordNo;
			}
			public void setMedicalRecordNo(long No) {
					this.medicalRecordNo = No;
			}
			/* Set my doctor to passed in doctor, and make
			 * sure original doctor has this patient removed from his
			   patients
			 */
			public void setDoctor(Doctor doctor) {
				
				if (!same(doctor)) {
					Doctor oldDoctor = this.doctor;
					this.doctor = doctor;
				if (oldDoctor != null) oldDoctor.removePatient(this);
			    if (doctor != null) doctor.addPatient(this);
				}
			}
			/* return a boolean indicating whether
			 * doctor argument is equal to my own doctor
			 */
			private boolean same(Doctor newDoctor ) {
				if (this.doctor == null) {
					return newDoctor == null;
				}
				else {
					return this.doctor.equals(newDoctor);
				}
			  }
			
			/* Set my prescription to prescription argument and make
			 * sure original prescription has this patient removed from its referenced
			   patients
			 */

			public void setPrescription(Prescription prescription) {
			    
			    if (same(prescription)) return;
     
			    
			     Prescription oldPrescription= this.prescription;
			    this.prescription = prescription;
			 
			    if (oldPrescription != null)
			      oldPrescription.setPatient(null);
			
			    if (prescription != null)
			       prescription.setPatient(this);
			  }
			
			/* return a boolean indicating whether
			 * prescription argument is equal to my own prescription
			 */
			
			private boolean same(Prescription newPrescription ) {
				if (this.prescription == null) {
					return newPrescription == null;
				}
				else {
					return this.prescription.equals(newPrescription);
				}
			  }
	        	
						  
			
		   /* Patients are equal if they have the same id */
			@Override
			public boolean equals(Object obj) {
				if (obj instanceof Patient) {
					Patient other = (Patient) obj;
					// Google Guava provides great utilities for equals too!
					
					System.out.println(id == other.id);
					return id == other.id;
						
				} else {
					return false;
				}
			}
			
			@Override
			public int hashCode() {
				// Google Guava provides great utilities for hashing
				return Objects.hashCode(id);
			}
}
