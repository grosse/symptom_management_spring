package org.grosse2011.symptom.auth;
/*
 * This implemenation of UserDetails service combines 
 * looks up both patients and doctors in data repository
 */
import org.grosse2011.symptom.repository.Doctor;
import org.grosse2011.symptom.repository.DoctorRepository;
import org.grosse2011.symptom.repository.Patient;
import org.grosse2011.symptom.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;



public class SymptomUserDetailsService implements UserDetailsService {
	@Autowired
	private PatientRepository patients;
	@Autowired
	private DoctorRepository doctors;
	

	public SymptomUserDetailsService() {
		super();
	}

	public SymptomUserDetailsService(PatientRepository patients, DoctorRepository doctors) {
		super();
		this.patients = patients;
		this.doctors = doctors;
	}
	@Override
	/*  
	 * (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 All users have hard coded password "pass"
	 */
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		  
		   //  user "admin" is hard coded and given PATIENT,DOCTOR,and ADMIN authorities
		   if (username.equals("admin")) return User.create("admin", "pass", "DOCTOR", "PATIENT", "ADMIN");
		   
				   
		
			try {
				/*
				users with names of the form patientx where x is an integer
				are mapped to patients in the Patient repository with id=x
				*/
				
				if (username.startsWith("patient")) {
			         long id = Long.parseLong(username.substring(username.lastIndexOf("t") + 1));
			         Patient p = patients.findOne(id);
			         if (p == null)  { throw new UsernameNotFoundException(username); }
                     return User.create(username, "pass", "PATIENT");
				}
				
				/*
				users with names of the form doctortx where x is an integer
				are mapped to entities in the Doctor repository with id=x
				*/
				
				else if (username.startsWith("doctor")){
					 long id = Long.parseLong(username.substring(username.lastIndexOf("r") + 1));
			          Doctor d = doctors.findOne(id);
			         if (d == null)  { throw new UsernameNotFoundException(username); }
                     return User.create(username, "pass", "DOCTOR");
				} else { throw new UsernameNotFoundException(username); }
			
			/* User was not in the Found */
				
			} catch (Exception e) { throw new UsernameNotFoundException(username); }
		
		
			
		
		
	
	
	}
}
   
