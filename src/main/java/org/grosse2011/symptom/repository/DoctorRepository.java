package org.grosse2011.symptom.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
/**
 * An interface for a repository that can store Doctor
 * objects.
 * 
 * Eric G.
 *
 */
@Repository
public interface DoctorRepository extends CrudRepository<Doctor, Long> {

}
