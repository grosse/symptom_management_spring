package org.grosse2011.symptom.repository;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/*
 * This entity represents an prescription of 0 or more pain medications
 */

@Entity
public class Prescription {
       @Id
       @GeneratedValue(strategy = GenerationType.AUTO)
       private long id;
       
       @OneToOne(cascade=CascadeType.ALL)
       @JoinColumn(name="patientId")
       private Patient patient;
       
       
       @OneToMany(cascade=CascadeType.ALL)
       @JoinColumn(name="checkinId")
       private Collection<PainMedication> medications = new ArrayList<PainMedication>();
       
       public long getId() {
           return this.id;
       }
       public void setId(long id) {
           this.id = id;
       }
       public PainMedication findMedication(String name) {
           Collection<PainMedication> m = this.medications;
           for (PainMedication painMedication : this.medications) {
        	   if (painMedication.getName().equals(name)) return painMedication;
        	   
           }
           return null;
       }
       public boolean addMedication(PainMedication painMedication) {
          
           if (painMedication == null) return false;
           painMedication = findMedication(painMedication.getName());
           if (painMedication == null) {
               medications.add(painMedication);
       	       return true;
           }
           return false;
       }
       // remove medication from list of medication repository
       public boolean removeMedication(String name) {
          
           if (name == null) return false;
           PainMedication painMedication = findMedication(name);
           if (painMedication != null) {
               medications.remove(painMedication);
        	   return true;
           }
           return false;
       }
       // Set the patient for this prescription and make sure
       // patient is associated with this prescription
       public void setPatient(Patient patient) {
           if (same(patient))  return ;
           Patient oldPatient = this.patient;
           this.patient = patient;
        	    
           if (oldPatient != null)
              oldPatient.setPrescription(null);
        	    //set myself as new owner
        	  if (patient != null)
        	      patient.setPrescription(this);
           this.patient = patient;
       }
       public Collection<PainMedication> getMedications() {
           return this.medications;
       }
       public Patient getPatient() {
           return this.patient;
       }
       // Given a patient, return true if am the same patient
       // false otherwiae
       private boolean same(Patient newPatient) {
           if (patient == null) return newPatient == null;
           return patient.equals(newPatient);
       }
       public Prescription() {
           this.medications = new ArrayList<PainMedication>();
       }
       public Prescription(PrescriptionDto prescriptionDto) {
           this.id = prescriptionDto.getId();
           this.medications = new ArrayList<PainMedication>();
           for (PainMedicationDto painMedicationDto : prescriptionDto.getPainMedications()) {       	  
               this.medications.add(new PainMedication(painMedicationDto));                                       
       } 
    }
}
