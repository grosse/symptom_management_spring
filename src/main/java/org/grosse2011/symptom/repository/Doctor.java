package org.grosse2011.symptom.repository;


import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
@Entity
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @OneToMany(cascade=CascadeType.ALL, mappedBy="doctor")
    private Collection<Patient> patients = new ArrayList<Patient>();
    private String firstName;
    private String lastName;
    		
    public long getId() {
    	return this.id;
    }
    			
    		  	
    public void setFirstName(String name) {
    	this.firstName = name;
    }
    	  	
    public void setLastName(String name) {
    	this.lastName = name;
    }	
    	
    public String getFirstName() {
        return this.firstName;
    }
    		   	
    public String getLastName() {
    	return this.lastName;
    }
    		   	
    /* Associate the patient passed in with this Doctor if not already associated */
    			
    public void addPatient(Patient patient) {
    	  		
    	if (!patients.contains(patient)) { 
       			patients.add(patient);
       			patient.setDoctor(this);
    	}
     }
    		   	
    /* dissassociate patient form this Doctor, diassociate doctor from this patient
     * for consistency
    */
    		   	
    public void removePatient(Patient patient)
    {
    	if (patients.contains(patient)) {
    		patients.remove(patient);
    	   	patient.setDoctor(null);
    	}
    }
    /* Return the assocatiated patients as a Collection of Patients */
    		   	
    public Collection<Patient> getPatients() {
    	return new ArrayList<Patient>(this.patients);
    }
    			
    public Doctor() {
        this.firstName = null;
        this.lastName = null;
    }
    /* Construct a doctor using a doctor facade as template */
    		
    public Doctor(DoctorDto dto) {
    	this.id = dto.id;
    	this.firstName = dto.firstName;
    	this.lastName = dto.lastName;
    }
    			
    /* another constructor */
    	    
    public Doctor(long id, String firstName, String lastName) {
       	this.id = id;
       	this.firstName = firstName;
       	this.lastName = lastName;
      	    			
     }
    		  
 }


