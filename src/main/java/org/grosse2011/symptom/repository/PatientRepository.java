package org.grosse2011.symptom.repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
/*
*
* An interface for a repository that can store Patients
* objects.
* 
* Eric G.
*
*/
@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {

}
